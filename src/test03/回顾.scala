package test03

object 回顾 {

  /**
    * 可变参数方法
    * def method1（str: String, a: Any*）
    */
  def method1(str: String, a: String*) = {
    var res = str
    for (b <- 0 until a.length) {
      res += a(b)
    }
  }

  /**
    * 默认值参数方法
    * def method2(a: Int = 6, x: String = "a")
    * method2()
    * method2(x=“b”, a=8)
    */
  def method2(a: Int = 6, x: String = "a") = {
    a + x
  }

  /**
    * 高阶函数：参数是函数 或者 返回值是函数
    * def method3(f: Int => Int, c: Int) = f(c)
    */
  def method3(f: Int => Int, c: Int) = f(c)

  val func = (a: Int) => {
    a * a
  }

  /**
    *
    * 柯里化
    * def method4(a: Int)(b: Int, c: Int)
    * method(0)(4, 5)
    */
  def method4(a: Int)(b: Int, c: Int) = {
    a + b + c
  }

  def plainOldSum(x: Int, y: Int) = x + y //非柯里化函数定义
  def curriedSum(x: Int)(y: Int) = x + y //柯里化使用多个参数列表
  /**
    * 偏函数：PartialFunction[参数类型，函数返回值类型]
    * def method5 :PartialFunction[Any, String] = {
    * case x: Int => x * 10
    * }
    */
  def method5: PartialFunction[Any, Int] = {
    case x: Int => x * 10
    case _ => -1
  }


  def main(args: Array[String]): Unit = {
    println(method3(func, 100))
    println(method4(1)(2, 3))
    println(plainOldSum(1, 5))
    println(curriedSum(2)(8))

    /*当你调用curriedSum (2)(8)时，实际上是依次调用两个普通函数（非柯里化函数）， 第一次调用使用一个参数x，返回一个函数类型的值，第二次使用参数y调用这个函数类型的值*/
    //等价于：
    def first(x: Int) = (y: Int) => x + y

    val second = first(2) //柯里化函数调用过程
    val res = second(8)
    println(res)
    //通过柯里函数curriedSum定义变量
    val onePlus = curriedSum(1) _ //_ 第二个参数列表的占位符，
    println(onePlus(2)) //传入的是第二个参数

    println(method5(5))
    println(method5(""))

    /**
      * 数组的定义
      * Array: 长度不可变，内容可变
      *
      * val arr = Array(1,4,6) => ??? => Array(10, 40 ,60)
      * 10 => function: 10 * 10 => 100
      * map => 映射  将集合进行某种操作，并且返回一个新的集合
      *     arr.map((x: Int) => x * 10)
      *     arr.map(x=> x * 10)
      *     arr.map(_ * 10)
      *
      * flatten 扁平化操作
      * Array[Array[String]] = Array(Array("1", "2"), Array("4", "5"))
      * 扁平化操作后：
      * Array[String] = Array("1", "2", "4", "5")
      *
      * flatMap
      * map + flatten
      *
      * foreach 对集合进行循环操作
      *     arr.foreach(f: Int => Unit)
      *
      *
      * WordCount
      *
      */

    val arr = Array(1, 4, 6)
    println(arr.map(x => x * 10).mkString(","))
    val arr1: Array[Array[String]] = Array(Array("1", "2"), Array("4", "5"))
    println(arr1.flatten.mkString(","))

    var flatss = arr1.flatMap(x=>x);
    println(flatss.mkString(","))
    arr.foreach(println)
  }
}
