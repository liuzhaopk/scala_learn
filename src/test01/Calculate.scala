package test01

object Calculate {

  //定义三个方法add1、add2、add3
  // add方法拥有2个Int类型的参数， 返回值为2个Int的和
  def add1(a: Int, b: Int) = {
    a + b
  }

  // add2方法拥有3个参数，第一个参数是一个函数， 第二个，第三个为Int类型的参数
  // 第一个参数：
  //     是拥有2个Int类型的参数，返回值为Int类型的函数
  def add2(f: (Int, Int) => Int, a: Int, b: Int) = {
    f(a, b) // f(1, 2) => 1 + 2
  }

  def add3(a: Int => Int, b: Int) = {
    a(b) + b // x * 10 + 6
  }


  //定义两个函数fxx和f1
  // fxx: (Int, Int) => Int
  val f2 = (a: Int, b: Int) => a + b


  val f1 = (x: Int) => x * 10

  def main(args: Array[String]): Unit = {
    println(add1(1, 2))
    println(add2(f2, 3, 4))
    println(add3(f1, 5))
  }

}
