package test04

trait Dog {

    val name: String = "dog"

    def run()

}
