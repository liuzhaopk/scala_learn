package test04

trait StudentTrait {

    type T

    def learn(s: T) = {
        println(s)
    }

}
